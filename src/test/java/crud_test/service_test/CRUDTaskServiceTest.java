package crud_test.service_test;

import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class})
public class CRUDTaskServiceTest extends AbstractTest {

    @Autowired
    @Qualifier("userService")
    IUserService userService;

    @Autowired
    @Qualifier("projectService")
    IProjectService projectService;

    @Autowired
    @Qualifier("taskService")
    ITaskService taskService;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Nullable
    Task task;

    @Before
    public void initEnvironment() {
        user = new User("test", "11111111");
        userService.persist(user);
        project = new Project();
        user.getProjects().add(project);
        project.setUser(user);
        project.setName("test");
        project.setDescription("test description");
        project.setStatus(Status.PLANNED);
        projectService.persist(project);
        task = new Task();
        task.setUser(user);
        user.getTasks().add(task);
        project.getTasks().add(task);
        task.setName("test");
        task.setDescription("test description");
        task.setStatus(Status.PLANNED);
        taskService.persist(task);
        projectService.merge(project);
        userService.merge(user);
    }

    @After
    public void cleanUp() {
        userService.removeEntity(user.getId());
    }

    @Test
    public void findByNamePositiveTest() {
        Assert.assertNotNull(taskService.getEntityByName(task.getName()));
        Assert.assertTrue(taskService.getEntityByName(task.getName()).getName().equals(task.getName()));
    }

    @Test
    public void findByNameNegativeTest() {
        Assert.assertNull(taskService.getEntityByName("test_"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertTrue(taskService.getEntityById(task.getId()).getId().equals(task.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findByIdNegativeTest() {
        Task test = taskService.getEntityById(UUID.randomUUID().toString());
        Assert.assertNull(test);
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertFalse(taskService.getAllEntities().isEmpty());
        List<Task> tasks = (ArrayList<Task>) taskService.getAllEntities();
        Assert.assertTrue(tasks.stream().filter(
                p -> p.getId().equals(task.getId()))
                .findFirst().get().getId().equals(task.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertFalse(taskService.getAllEntities().isEmpty());
        List<Task> tasks = (ArrayList<Task>) taskService.getAllEntities();
        Assert.assertFalse(tasks.stream().filter(
                p -> p.getId().equals(new Task().getId()))
                .findFirst().get().getId().equals(new Task().getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertTrue(taskService.getEntityById(task.getId()).getId().equals(task.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void createNegativeTest() {
        Task test = taskService.getEntityById(UUID.randomUUID().toString());
        Assert.assertNull(test);
    }

    @Test
    public void mergePositiveTest() {
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertTrue(taskService.getEntityById(task.getId()).getId().equals(task.getId()));
        task.setName("olo-lo");
        taskService.merge(task);
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertTrue(taskService.getEntityById(task.getId()).getName().equals(task.getName()));
    }

    @Test
    public void mergeNegativeTest() {
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertTrue(taskService.getEntityById(task.getId()).getId().equals(task.getId()));
        task.setName("olo-lo");
        taskService.merge(task);
        Assert.assertNotNull(taskService.getEntityById(task.getId()));
        Assert.assertFalse(taskService.getEntityById(task.getId()).getName().equals("test"));
    }

}