package crud_test.service_test;

import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class})
public class CRUDProjectServiceTest extends AbstractTest {

    @Autowired
    @Qualifier("userService")
    IUserService userService;

    @Autowired
    @Qualifier("projectService")
    IProjectService projectService;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Before
    public void initEnvironment() {
        user = new User("test", "11111111");
        userService.persist(user);
        project = new Project();
        user.getProjects().add(project);
        project.setUser(user);
        project.setName("test");
        project.setDescription("test description");
        project.setStatus(Status.PLANNED);
        projectService.persist(project);
        userService.merge(user);
    }

    @After
    public void cleanUp() {
        userService.removeEntity(user.getId());
    }

    @Test
    public void findByNamePositiveTest() {
        Assert.assertNotNull(projectService.getEntityByName(project.getName()));
        Assert.assertTrue(projectService.getEntityByName(project.getName()).getName().equals(project.getName()));
    }

    @Test
    public void findByNameNegativeTest() {
        Assert.assertNull(projectService.getEntityByName("test_"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertTrue(projectService.getEntityById(project.getId()).getId().equals(project.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findByIdNegativeTest() {
        Project test = projectService.getEntityById(UUID.randomUUID().toString());
        Assert.assertNull(test);
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertFalse(projectService.getAllEntities().isEmpty());
        List<Project> projects = (ArrayList<Project>) projectService.getAllEntities();
        Assert.assertTrue(projects.stream().filter(
                p -> p.getId().equals(project.getId()))
                .findFirst().get().getId().equals(project.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertFalse(projectService.getAllEntities().isEmpty());
        List<Project> projects = (ArrayList<Project>) projectService.getAllEntities();
        Assert.assertFalse(projects.stream().filter(
                p -> p.getId().equals(new Project().getId()))
                .findFirst().get().getId().equals(new Project().getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertTrue(projectService.getEntityById(project.getId()).getId().equals(project.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void createNegativeTest() {
        Project test = projectService.getEntityById(UUID.randomUUID().toString());
        Assert.assertNull(test);
    }

    @Test
    public void mergePositiveTest() {
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertTrue(projectService.getEntityById(project.getId()).getId().equals(project.getId()));
        project.setName("olo-lo");
        projectService.merge(project);
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertTrue(projectService.getEntityById(project.getId()).getName().equals(project.getName()));
    }

    @Test
    public void mergeNegativeTest() {
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertTrue(projectService.getEntityById(project.getId()).getId().equals(project.getId()));
        project.setName("olo-lo");
        projectService.merge(project);
        Assert.assertNotNull(projectService.getEntityById(project.getId()));
        Assert.assertFalse(projectService.getEntityById(project.getId()).getName().equals("test"));
    }

}