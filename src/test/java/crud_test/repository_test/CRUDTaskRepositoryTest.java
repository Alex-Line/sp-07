package crud_test.repository_test;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.repository.ITaskRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

public class CRUDTaskRepositoryTest extends AbstractTest {

    @Autowired
    @Qualifier("user_repository")
    IUserRepository userRepository;

    @Autowired
    @Qualifier("project_repository")
    IProjectRepository projectRepository;

    @Autowired
    @Qualifier("task_repository")
    ITaskRepository taskRepository;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Nullable
    Task task;

    @Before
    public void initEnvironment() {
        user = new User("user", "11111111");
        userRepository.save(user);
        project = new Project();
        project.setUser(user);
        user.getProjects().add(project);
        project.setName("project");
        project.setDescription("project description");
        project.setStatus(Status.PLANNED);
        projectRepository.save(project);
        task = new Task();
        user.getTasks().add(task);
        project.getTasks().add(task);
        task.setProject(project);
        task.setUser(user);
        task.setDescription("test description");
        task.setName("test");
        task.setStatus(Status.PLANNED);
        taskRepository.save(task);
    }

    @After
    public void cleanUp() {
        userRepository.delete(user);
    }

    @Test
    public void findByNamePositiveTest() {
        Assert.assertNotNull(taskRepository.findOneByName(task.getName()));
        Assert.assertTrue(taskRepository.findOneByName(task.getName()).getId().equals(task.getId()));
    }

    @Test
    public void findByNameNegativeTest() {
        Assert.assertNull(taskRepository.findOneByName("task test"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(taskRepository.findById(task.getId()));
        Assert.assertTrue(taskRepository.findById(task.getId()).get().getId().equals(task.getId()));
    }

    @Test
    public void findByIdNegativeTest() {
        Assert.assertSame(Optional.empty(), taskRepository.findById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertNotNull(taskRepository.findAll());
        List<Task> tasks = (List<Task>) taskRepository.findAll();
        Assert.assertTrue(tasks.size() > 0);
        Assert.assertTrue(tasks.stream().filter(
                p -> p.getId().equals(task.getId()))
                .findFirst().get().getId().equals(task.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertNotNull(taskRepository.findAll());
        List<Task> tasks = (List<Task>) taskRepository.findAll();
        Assert.assertTrue(tasks.size() > 0);
        Task testTask = new Task();
        Assert.assertFalse(tasks.stream().filter(
                p -> p.getId().equals(testTask.getId()))
                .findFirst().get().getId().equals(testTask.getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(taskRepository.findOneByName(task.getName()));
        Assert.assertTrue(taskRepository.findOneByName(task.getName()).getId().equals(task.getId()));
    }

    @Test
    public void createNegativeTest() {
        Assert.assertNull(taskRepository.findOneByName("task test"));
    }

    @Test
    public void savePositiveTest() {
        Assert.assertNotNull(taskRepository.findById(task.getId()));
        Assert.assertTrue(taskRepository.findById(task.getId()).get().getId().equals(task.getId()));
        task.setStatus(Status.DONE);
        taskRepository.save(task);
        Assert.assertNotNull(taskRepository.findById(task.getId()));
        Assert.assertTrue(taskRepository.findById(task.getId()).get().getStatus().equals(task.getStatus()));
    }

    @Test
    public void saveNegativeTest() {
        Assert.assertNotNull(taskRepository.findById(task.getId()));
        Assert.assertTrue(taskRepository.findById(task.getId()).get().getId().equals(task.getId()));
        task.setStatus(Status.DONE);
        taskRepository.save(task);
        Assert.assertNotNull(taskRepository.findById(task.getId()));
        Assert.assertFalse(taskRepository.findById(task.getId()).get().getStatus().equals(Status.PLANNED));
    }


}