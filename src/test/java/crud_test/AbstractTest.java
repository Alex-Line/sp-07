package crud_test;

import com.iteco.linealex.config.HibernateConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class,})
public class AbstractTest {

    @Test
    public void alwaysTrueCheck(){
        Assert.assertNull(null);
    }

}