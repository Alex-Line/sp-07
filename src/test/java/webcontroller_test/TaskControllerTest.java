package webcontroller_test;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.repository.ITaskRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.config.HibernateConfig;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, HibernateConfig.class})
public class TaskControllerTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IProjectRepository projectRepository;

    @Autowired
    ITaskRepository taskRepository;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Nullable
    Task task;

    @Before
    public void initEnvironment() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        user = new User("test", "11111111");
        userRepository.save(user);
        project = new Project();
        project.setUser(user);
        user.getProjects().add(project);
        projectRepository.save(project);
        userRepository.save(user);
        task = new Task();
        task.setUser(user);
        task.setProject(project);
        user.getTasks().add(task);
        project.getTasks().add(task);
        taskRepository.save(task);
        projectRepository.save(project);
        userRepository.save(user);
    }

    @After
    public void cleanUp() {
        SecurityContextHolder.clearContext();
        userRepository.delete(user);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void getTaskListTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/list")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("tasklist"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/tasklist.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void viewTaskTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/view")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("taskview"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/taskview.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void editTaskTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/edit")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("taskedit"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/taskedit.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void createTaskTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED")
                .param("userLogin", "test")
                .param("projectName", "unnamed"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("taskcreate"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/taskcreate.jsp"));
    }

}