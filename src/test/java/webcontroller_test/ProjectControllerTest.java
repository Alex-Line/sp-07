package webcontroller_test;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.config.HibernateConfig;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
import java.util.NoSuchElementException;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, HibernateConfig.class})
public class ProjectControllerTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IProjectRepository projectRepository;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Before
    public void initEnvironment() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        user = new User("test", "11111111");
        userRepository.save(user);
        project = new Project();
        project.setUser(user);
        user.getProjects().add(project);
        projectRepository.save(project);
        userRepository.save(user);
    }

    @After
    public void cleanUp() {
        SecurityContextHolder.clearContext();
        userRepository.delete(user);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void getProjectListTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/list"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("projectlist"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/projectlist.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void viewProjectTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/view")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("projectview"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/projectview.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void editProjectTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/edit")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("projectedit"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/projectedit.jsp"));
    }

    @Test(expected = NoSuchElementException.class)
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void deleteProjectTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/delete")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is(302));
        Project test = projectRepository.findById(project.getId()).get();
        Assert.assertNull(test);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void createProjectTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("projectcreate"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/projectcreate.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void selectProjectTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/select")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

}