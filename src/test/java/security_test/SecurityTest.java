package security_test;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.repository.ITaskRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.config.HibernateConfig;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, HibernateConfig.class})
public class SecurityTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    IUserRepository userRepository;

    @Autowired
    IProjectRepository projectRepository;

    @Autowired
    ITaskRepository taskRepository;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Nullable
    Task task;

    @Before
    public void initEnvironment() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        user = new User("test", "11111111");
        userRepository.save(user);
        project = new Project();
        project.setUser(user);
        user.getProjects().add(project);
        projectRepository.save(project);
        userRepository.save(user);
        task = new Task();
        task.setUser(user);
        task.setProject(project);
        user.getTasks().add(task);
        project.getTasks().add(task);
        taskRepository.save(task);
        projectRepository.save(project);
        userRepository.save(user);
    }

    @After
    public void cleanUp() {
        SecurityContextHolder.clearContext();
        //taskRepository.delete(task);
        //projectRepository.delete(project);
        userRepository.delete(user);
    }

    @Test
    public void getHomePageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void getUserListPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/list"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void getUserListNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/list"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void getUserListNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/list"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void viewUserPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/view")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void viewUserNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/view")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void viewUserNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/view")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void editUserPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/edit")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void editUserNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/edit")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void editUserNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/edit")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void deleteUserNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/delete")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(302));
    }

    @Test(expected = Exception.class)
    public void deleteUserNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/delete")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(302));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void createUserPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/create")
                .param("login", "login")
                .param("password", "password"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void createUserNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/create")
                .param("login", "login")
                .param("password", "password"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void createUserNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/create")
                .param("login", "login")
                .param("password", "password"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void selectUserPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/select")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test(expected = Exception.class)
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void selectUserNegativeTest1() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/select")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test(expected = Exception.class)
    public void selectUserNegativeTest2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/select")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void getProjectListPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/list"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void getProjectListNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/list"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void viewProjectPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/view")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void viewProjectNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/view")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void editProjectPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/edit")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void editProjectNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/edit")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void deleteProjectNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/delete")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is(302));
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void createProjectPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void createProjectNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void selectProjectPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/select")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test(expected = Exception.class)
    public void selectProjectNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/project/select")
                .param("projectId", project.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void getTaskListPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/list")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void getTaskListNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/list")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void viewTaskPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/view")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void viewTaskNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/view")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void editTaskPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/edit")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void editTaskNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/edit")
                .param("taskId", task.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @WithMockUser(username = "user", roles = "ORDINARY_USER")
    public void createTaskPositiveTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED")
                .param("userLogin", "test")
                .param("projectName", "unnamed"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test(expected = Exception.class)
    public void createTaskNegativeTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/task/create")
                .param("name", "name")
                .param("description", "description")
                .flashAttr("dateStart", new Date())
                .flashAttr("dateFinish", new Date())
                .param("status", "PLANNED")
                .param("userLogin", "test")
                .param("projectName", "unnamed"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}