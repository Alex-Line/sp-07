package com.iteco.linealex.service;

import com.iteco.linealex.model.AbstractTMEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public abstract class AbstractTMService<T extends AbstractTMEntity> extends AbstractService<T> {

    @Nullable
    @Override
    public abstract T getEntityById(@Nullable final String entityId);

    @NotNull
    @Override
    public abstract Collection<T> getAllEntities();

    @Override
    public abstract void persist(@Nullable T entity);

    @Override
    public abstract void removeEntity(@Nullable final String entityId);

    public abstract void removeAllEntities(@Nullable final String userId);

    @Override
    public abstract void removeAllEntities();

    @Override
    public abstract void merge(@Nullable final T entity);

    @Nullable
    public abstract T getEntityByName(@Nullable final String entityName);

    @Nullable
    public abstract T getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    );

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    );

    @NotNull
    public abstract Collection<T> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate();

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate();

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus();

    @NotNull
    public abstract Collection<T> getAllEntitiesSortedByStatus(@Nullable final String userId);

    @NotNull
    public abstract Collection<T> getAllEntities(@Nullable final String userId);

}