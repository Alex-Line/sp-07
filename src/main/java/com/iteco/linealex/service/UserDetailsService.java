package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.model.Role;
import com.iteco.linealex.model.User;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@NoArgsConstructor
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        if (userName == null || userName.isEmpty()) throw new UsernameNotFoundException("Wrong name!");
        @Nullable final User user = userRepository.findOneByLogin(userName);
        if (user == null) throw new UsernameNotFoundException("There is no such user!");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(userName);
        builder.password(user.getHashPassword());
        @NotNull final List<Role> userRoles = new ArrayList<>(user.getRoles());
        @NotNull final List<String> roles = new ArrayList<>();
        for (Role role : userRoles) {
            roles.add(role.toString());
        }
        builder.roles(roles.toArray(new String[] {}));
        return builder.build();
    }
}