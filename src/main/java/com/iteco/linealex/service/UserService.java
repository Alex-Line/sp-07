package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.IRoleRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

@Transactional
@Service("userService")
public final class UserService extends AbstractService<User> implements IUserService {

    @Value("${ADMIN}")
    private String adminLogin;

    @Value("${ADMIN_PASSWORD}")
    private String adminPassword;

    @Value("${USER}")
    private String userLogin;

    @Value("${USER_PASSWORD}")
    private String userPassword;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IRoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @PostConstruct
    void init() throws NoSuchAlgorithmException {
        roleRepository.deleteAll();
        userRepository.deleteAll();
        @NotNull final Role adminRole = new Role();
        adminRole.setRoleType(RoleType.ADMINISTRATOR);
        @NotNull final Role ordinaryUserRole = new Role();
        ordinaryUserRole.setRoleType(RoleType.ORDINARY_USER);
        @NotNull final User admin = new User();
        admin.setLogin(adminLogin);
        admin.setHashPassword(passwordEncoder.encode(adminPassword));
        admin.getRoles().add(adminRole);
        admin.getRoles().add(ordinaryUserRole);
        adminRole.setUser(admin);
        ordinaryUserRole.setUser(admin);
        userRepository.save(admin);
        roleRepository.save(adminRole);
        roleRepository.save(ordinaryUserRole);
        @NotNull final Role userRole = new Role();
        ordinaryUserRole.setRoleType(RoleType.ORDINARY_USER);
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setHashPassword(passwordEncoder.encode(userPassword));
        user.getRoles().add(userRole);
        userRole.setUser(user);
        userRepository.save(user);
    }

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        return userRepository.findById(entityId).get();
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() {
        return (Collection<User>) userRepository.findAll();
    }

    @Override
    @Transactional
    public void persist(@Nullable final User entity) {
        if (entity == null) return;
        if (entity.getLogin().isEmpty()) return;
        if (entity.getHashPassword() == null || entity.getHashPassword().isEmpty()) return;
        userRepository.save(entity);
    }

    @Override
    public void persist(@NotNull Collection<User> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            persist(user);
        }
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null) return;
        userRepository.deleteById(entityId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        userRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(@Nullable final User entity) {
        if (entity == null) return;
        userRepository.save(entity);
    }

    @Transactional
    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new Exception("User is not log in");
        if (oldPassword == null || oldPassword.isEmpty()) throw new Exception("Wrong password");
        @NotNull final String hashOldPassword = passwordEncoder.encode(oldPassword);
        if (selectedUser.getHashPassword() == null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new Exception("Wrong password");
        if (newPassword == null || newPassword.isEmpty()) throw new Exception("Wrong password");
        if (newPassword.length() < 8) throw new Exception("Short password");
        @NotNull final String hashNewPassword = passwordEncoder.encode(newPassword);
        selectedUser.setHashPassword(hashNewPassword);
        userRepository.save(selectedUser);
    }

    @Nullable
    public User getUser(@Nullable final String login) {
        return userRepository.findOneByLogin(login);
    }

}