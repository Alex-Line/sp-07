package com.iteco.linealex.api.service;

import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    @NotNull
    Collection<Project> getAllEntities(@Nullable final String userId);

    @Nullable
    Project getEntityByName(@Nullable final String entityName);

    @Nullable
    Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    );

    void removeAllEntities(@Nullable final String userId);

    @NotNull
    Collection<Project> getAllEntitiesByName(@Nullable final String pattern);

    @NotNull
    Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    );

    @NotNull
    Collection<Project> getAllEntitiesSortedByStartDate();

    @NotNull
    Collection<Project> getAllEntitiesSortedByStartDate(@Nullable final String userId);

    @NotNull
    Collection<Project> getAllEntitiesSortedByFinishDate();

    @NotNull
    Collection<Project> getAllEntitiesSortedByFinishDate(@Nullable final String userId);

    @NotNull
    Collection<Project> getAllEntitiesSortedByStatus();

    @NotNull
    Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId);

}