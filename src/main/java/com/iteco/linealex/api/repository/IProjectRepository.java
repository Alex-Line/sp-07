package com.iteco.linealex.api.repository;

import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository(value = "project_repository")
public interface IProjectRepository extends CrudRepository<Project, String> {

    void deleteAllByUserId(String userId);

    Project findOneByName(String entityName);

    Project findOneByNameAndUserId(String userId, String entityName);

    Collection<Project> findAllByNameAndUserId(String userId, String pattern);

    Collection<Project> findAllByNameLike(String pattern);

    @Query("SELECT p FROM Project p ORDER BY p.dateStart")
    Collection<Project> findAllSortedByDateStart();

    @Query("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.dateStart")
    Collection<Project> findAllByUserIdSortedByDateStart(@Param("userId") @NotNull final String userId);

    @Query("SELECT p FROM Project p ORDER BY p.dateFinish")
    Collection<Project> findAllSortedByDateFinish();

    @Query("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.dateFinish")
    Collection<Project> findAllByUserIdSortedByDateFinish(@Param("userId") @NotNull final String userId);

    @Query("SELECT p FROM Project p ORDER BY p.status")
    Collection<Project> findAllSortedByStatus();

    @Query("SELECT p FROM Project p WHERE p.user.id= :userId ORDER BY p.status")
    Collection<Project> findAllByUserIdSortedByStatus(@Param("userId") @NotNull final String userId);

    Collection<Project> findAllByUserId(String userId);

}