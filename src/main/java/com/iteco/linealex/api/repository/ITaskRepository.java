package com.iteco.linealex.api.repository;

import com.iteco.linealex.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository("task_repository")
public interface ITaskRepository extends CrudRepository<Task, String> {

    Collection<Task> findAllByUserId(String userId);

    Collection<Task> findAllByUserIdAndProjectId(String userId, String projectId);

    Task findOneByName(String entityName);

    Task findOneByNameAndUserId(String userId, String entityName);

    Collection<Task> findAllByUserIdAndNameLike(String userId, String pattern);

    Collection<Task> findAllByNameLike(String pattern);

    @Query("SELECT t FROM Task t ORDER BY t.dateStart")
    Collection<Task> findAllSortedByDateStart();

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId ORDER BY t.dateStart")
    Collection<Task> findAllByUserIdSortedByDateStart(@Param("userId") @NotNull final String userId);

    @Query("SELECT t FROM Task t ORDER BY t.dateFinish")
    Collection<Task> findAllSortedByDateFinish();

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId ORDER BY t.dateFinish")
    Collection<Task> findAllByUserIdSortedByDateFinish(@Param("userId") @NotNull final String userId);

    @Query("SELECT t FROM Task t ORDER BY t.status")
    Collection<Task> findAllSortedByStatus();

    @Query("SELECT t FROM Task t WHERE t.user.id= :userId ORDER BY t.status")
    Collection<Task> findAllByUserIdSortedByStatus(@Param("userId") @NotNull final String userId);

    void deleteAllByUserId(String userId);

    void deleteAllByUserIdAndProjectId(String userId, String projectId);

    Task findOneByNameAndUserIdAndProjectId(String userId, String projectId, String taskName);

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId AND t.project.id= :projectId " +
            "AND (t.name LIKE '%:pattern%'" +
            "OR t.description LIKE '%:pattern%')")
    Collection<Task> findAllByUserIdAndProjectIdAndByNameLike(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId,
            @Param("pattern") @NotNull final String pattern
    );

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId AND t.project.id= :projectId " +
            "ORDER BY t.dateStart")
    Collection<Task> findAllByUserIdAndProjectIdSortedByDateStart(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId
    );

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId AND t.project.id= :projectId " +
            "ORDER BY t.dateFinish")
    Collection<Task> findAllByUserIdAndProjectIdSortedByDateFinish(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId
    );

    @Query("SELECT t FROM Task t " +
            "WHERE t.user.id= :userId AND t.project.id= :projectId " +
            "ORDER BY t.status")
    Collection<Task> findAllByUserIdAndProjectIdSortedByStatus(
            @Param("userId") @NotNull final String userId,
            @Param("projectId") @NotNull final String projectId
    );

}