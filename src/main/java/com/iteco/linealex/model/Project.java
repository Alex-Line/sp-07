package com.iteco.linealex.model;

import com.iteco.linealex.dto.ProjectDto;
import com.iteco.linealex.util.ApplicationUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "projects")
public final class Project extends AbstractTMEntity implements Serializable {

    //@NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    List<Task> tasks = new ArrayList<>();

    @NotNull
    public static ProjectDto toProjectDto(
            @NotNull final Project project
    ) {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setDescription(project.getDescription());
        projectDto.setStatus(project.getStatus());
        projectDto.setDateStart(project.getDateStart());
        projectDto.setDateFinish(project.getDateFinish());
        projectDto.setUserId(project.getUser().getId());
        return projectDto;
    }

    @NotNull
    public static List<ProjectDto> toProjectsDto(
            @NotNull final List<Project> projects
    ) {
        @NotNull final List<ProjectDto> result = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            result.add(toProjectDto(project));
        }
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project: " + name +
                " { ID = " + super.getId() +
                ",\n    description = '" + description + '\'' +
                ",\n    Start Date = " + ApplicationUtils.formatDateToString(dateStart) +
                ",\n    Finish Date = " + ApplicationUtils.formatDateToString(dateFinish) +
                ",\n    Status = " + status.getName() +
//                ",\n    User ID = " + user.getId() +
                " }";
    }

}