package com.iteco.linealex.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.iteco.linealex.dto.UserDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "users")
public final class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    @Basic(optional = false)
    private String login = "unnamed";

    @Nullable
    @Basic(optional = false)
    private String hashPassword;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<Role> roles = new ArrayList<>();

    public User(
            @NotNull final String login,
            @NotNull final String hashPassword
    ) {
        this.login = login;
        this.hashPassword = hashPassword;
    }

    @Nullable
    public static UserDto toUserDto(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setHashPassword(user.getHashPassword());
        for (Role role : user.getRoles()) {
            userDto.getRoles().add(role.getId());
        }
        userDto.setLogin(user.getLogin());
        return userDto;
    }

    @NotNull
    public static List<UserDto> toUsersDto(@NotNull final List<User> users) {
        @NotNull final List<UserDto> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            result.add(toUserDto(user));
        }
        return result;
    }

    @NotNull
    @Override
    public String toString() {
        return "User " + login +
                ",\n     roles = " + roles +
                ",\n     id = " + super.getId() +
                ",\n     hashPassword = " + hashPassword;
    }

}