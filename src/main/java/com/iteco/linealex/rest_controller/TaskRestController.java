package com.iteco.linealex.rest_controller;

import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.dto.TaskDto;
import com.iteco.linealex.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("api/task")
public class TaskRestController {

    @Autowired
    ITaskService taskService;

    //C in CRUD
    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(
            @RequestBody final TaskDto taskDto
    ) throws Exception {
        @NotNull final Task task = TaskDto.toTask(taskDto);
        taskService.persist(task);
    }

    //R in CRUD
    @PostMapping(value = {"list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<TaskDto> getAllTasks() {
        return Task.toTasksDto(new ArrayList<>(taskService.getAllEntities()));
    }

    //R in CRUD
    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDto getTaskById(@PathVariable final String id) {
        return Task.toTaskDto(taskService.getEntityById(id));
    }

    //U in CRUD
    @PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(
            @RequestBody final TaskDto taskDto
    ) throws Exception {
        @Nullable Task task = taskService.getEntityById(taskDto.getId());
        if (task != null) task = TaskDto.toTask(taskDto);
        taskService.merge(task);
    }

    //D in CRUD
    @DeleteMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable final String id) {
        taskService.removeEntity(id);
    }

}