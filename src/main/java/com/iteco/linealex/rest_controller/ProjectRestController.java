package com.iteco.linealex.rest_controller;

import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.dto.ProjectDto;
import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("api/project")
public class ProjectRestController {

    @Autowired
    IProjectService projectService;

    //C in CRUD
    @PostMapping(value = "create", produces = MediaType.APPLICATION_JSON_VALUE)
    public void create(
            @RequestBody final ProjectDto projectDto
    ) throws Exception {
        @NotNull final Project project = ProjectDto.toProject(projectDto);
        projectService.persist(project);
    }

    //R in CRUD
    @PostMapping(value = {"list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Collection<ProjectDto> getAllProjects() {
        return Project.toProjectsDto(new ArrayList<>(projectService.getAllEntities()));
    }

    //R in CRUD
    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto getProjectById(@PathVariable final String id) {
        return Project.toProjectDto(projectService.getEntityById(id));
    }

    //U in CRUD
    @PutMapping(value = "update", produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(
            @RequestBody final ProjectDto projectDto
    ) throws Exception {
        @Nullable Project project = projectService.getEntityById(projectDto.getId());
        if (project != null) project = ProjectDto.toProject(projectDto);
        projectService.merge(project);
    }

    //D in CRUD
    @DeleteMapping(value = "delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable final String id) {
        projectService.removeEntity(id);
    }

}