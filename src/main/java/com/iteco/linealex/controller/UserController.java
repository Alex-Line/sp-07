package com.iteco.linealex.controller;

import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.NoSuchAlgorithmException;

@Controller
public class UserController {

    @Autowired
    IUserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping("/user/list")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ModelAndView listUsers() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("userlist");
        modelAndView.addObject("users", userService.getAllEntities());
        return modelAndView;
    }

    @RequestMapping("/user/view")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ModelAndView view(@NotNull final String userId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("userview");
        modelAndView.addObject("user", userService.getEntityById(userId));
        return modelAndView;
    }

    @RequestMapping("/user/edit")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ModelAndView edit(@NotNull final String userId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("useredit");
        @Nullable final User user = userService.getEntityById(userId);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping("/user/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String delete(@ModelAttribute("userId") String userId) {
        userService.removeEntity(userId);
        return "redirect:/user/list";
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public ModelAndView create(
            @ModelAttribute("login") String login,
            @ModelAttribute("password") String password
    ) throws NoSuchAlgorithmException {
        @NotNull final ModelAndView modelAndView = new ModelAndView("usercreate");
        @NotNull final User createdUser = new User();
        createdUser.setLogin(login);
        createdUser.setHashPassword(passwordEncoder.encode(password));
        @NotNull final Role role = new Role();
        role.setRoleType(RoleType.ORDINARY_USER);
        role.setUser(createdUser);
        createdUser.getRoles().add(role);
        modelAndView.addObject("user", createdUser);
        return modelAndView;
    }

    @PreAuthorize("hasRole('ADMINISTRATOR')")
    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("user") User user) {
        @NotNull final User createdUser = new User();
        createdUser.setLogin(user.getLogin());
        createdUser.setHashPassword(user.getHashPassword());
        createdUser.setRoles(user.getRoles());
        userService.persist(user);
        return "redirect:/user/list";
    }

    @RequestMapping("/user/select")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ModelAndView select(@ModelAttribute("userId") String userId) {
        @NotNull final ModelAndView modelAndView =
                new ModelAndView("project_vs_task", "userId", userId);
        return modelAndView;
    }

}