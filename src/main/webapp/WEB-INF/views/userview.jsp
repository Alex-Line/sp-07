<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>View User</h2>
        <form:form method="get" modelAttribute="user">
            <table border="0" cellpadding="5">
                <tr>
                    <td>ID: </td>
                    <td>${user.id}</td>
                </tr>
                <tr>
                    <td>Login: </td>
                    <td>${user.login}</td>
                </tr>
                <tr>
                    <td>Roles: </td>
                    <td>${user.roles}</td>
                </tr>
            </table>
        </form:form>
        <form method="get" action="/user/list">
            <button style="width:300x;height:50px" type="submit">Ok</button>
        </form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>