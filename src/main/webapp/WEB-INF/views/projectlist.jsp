<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
<div align="center">
    <h1>Project List</h1>
    <h3><a href="/project/create?userId=${userId}">New Project</a></h3>
    <h3>User ID: ${userId}</h3>
    <input name="userId" type="hidden" value="${userId}">
    <table border="1" cellpadding="5">
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Description</td>
            <td>Start date</td>
            <td>Finish date</td>
            <td>Status</td>
            <td>User ID</td>
            <td>Action</td>
        </tr>
            <c:forEach items="${projects}" var="project">
                <tr>
                    <td>${project.id}</td>
                    <td>${project.name}</td>
                    <td>${project.description}</td>
                    <td>${project.dateStart}</td>
                    <td>${project.dateFinish}</td>
                    <td>${project.status}</td>
                    <td>${userId}</td>
                    <td>
                        <a href="/project/select?userId=${userId}&projectId=${project.id}">select</a>
                        <a href="/project/view?userId=${userId}&projectId=${project.id}">view</a>
                        <a href="/project/edit?userId=${userId}&projectId=${project.id}">edit</a>
                        <a href="/project/delete?userId=${userId}&projectId=${project.id}">delete</a>
                    </td>
                 </tr>
            </c:forEach>
    </table>
    <a href="/logout"> Sign out</a>
</div>
</body>
</html>