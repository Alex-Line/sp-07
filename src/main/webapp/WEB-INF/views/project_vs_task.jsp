<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h1>Choose the sort of item to work at</h1>
        <h3>User ID: ${userId}</h3>
        <table border="0" cellpadding="70">
            <tr>
                <td>
                    <form method="post" action="/project/list">
                    <%
                        request.setAttribute("userId", "${userId}");
                    %>
                        <input name="userId" type="hidden" value="${userId}">
                        <button style="font-size:20px;height:200px;width:200px" type="submit">   Projects   </button>
                    </form>
                </td>
                <td></td>
                <td>
                    <form method="post" action="/task/list">
                    <input name="userId" type="hidden" value="${userId}">
                    <input name="projectId" type="hidden" value="${projectId}">
                        <button style="font-size:20px;height:200px;width:200px" type="submit">   Tasks   </button>
                    </form>
                </td>
            </tr>
        </table>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>