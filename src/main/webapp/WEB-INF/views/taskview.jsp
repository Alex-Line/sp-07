<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>View Task</h2>
        <form:form method="get" modelAttribute="task">
            <table border="0" cellpadding="5">
                <tr>
                    <td>ID: </td>
                    <td>${task.id}</td>
                </tr>
                <tr>
                    <td>Name: </td>
                    <td>${task.name}</td>
                </tr>
                <tr>
                    <td>Description: </td>
                    <td>${task.description}</td>
                </tr>
                <tr>
                    <td>Start date: </td>
                    <td>${task.dateStart}</td>
                </tr>
                <tr>
                    <td>Finish date: </td>
                    <td>${task.dateFinish}</td>
                </tr>
                <tr>
                    <td>Status: </td>
                    <td>${task.status}</td>
                </tr>
                <tr>
                    <td>User ID: </td>
                    <td>${task.user.id}</td>
                </tr>
                <tr>
                    <td>Project ID: </td>
                    <td>${task.project.id}</td>
                </tr>
            </table>
        </form:form>
        <form method="get" action="/task/list">
            <button style="width:300x;height:50px" type="submit">Ok</button>
        </form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>