https://gitlab.com/Alex-Line/sp-06.git

# Task Manager SP-06

## Developer
Aleksandr Linev

E-mail: uranus_123@mail.ru

## Software

* Maven 3.6.1
* Java 1.8
* MYSQL 8.0.18
* Spring 5.2.3.RELEASE
* PostgreSQL 42.2.12
* Tomcat 9.0.35
* Mockito 1.10.19
* JRE
* IDE IntelliJ IDEA CE

## Build commands

    mvn clean
    mvn install

## Run command

    java -jar target/taskmanager.war

